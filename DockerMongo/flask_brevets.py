"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)
"""
import os
import flask
from flask import request, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = 'ouch' 
###
# Pages
###

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route("/")
@app.route("/index")
def index():
    db.tododb.remove({})
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """ Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)
    brevet_dist = request.args.get('brevet_dist', type=int)

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    temp = begin_date + " " + begin_time
    startInfo = arrow.get(temp).isoformat()


    open_time = acp_times.open_time(km, brevet_dist, startInfo)
    close_time = acp_times.close_time(km, brevet_dist, startInfo)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

#############

@app.route("/first", methods=["POST"])
def submit():
    db.tododb.remove({})
    open_li = request.form.getlist('open')
    close_li = request.form.getlist('close')
    for i in range(len(open_li)):
        if open_li[i] != "":
            result = {'name': open_li[i], 'disc': close_li[i]}          
            db.tododb.insert_one(result)
    return flask.render_template("calc.html")


@app.route("/new", methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if len(items) == 0:
        return flask.render_template('empty.html')
    return flask.render_template('success.html', items=items)              


app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
