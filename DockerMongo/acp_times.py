"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """ 
    if (isinstance(control_dist_km, str)):
        control_dist_km = 0.0     
    control_dist_km = abs(control_dist_km)
    result =  arrow.get(brevet_start_time)
    max_speed = 0
    locations = {200:34, 400:32, 600:30, 1000:28}

    if (control_dist_km > brevet_dist_km):
        speed = locations[brevet_dist_km]
        hour = brevet_dist_km // speed
        minute = round(((brevet_dist_km / speed) - hour) * 60)
        result = result.shift(hours=+hour, minutes=+minute)
        return result.isoformat()

    for speed in locations:
        if control_dist_km <= speed:
            max_speed += locations[speed]
            break
     
    hour = (control_dist_km // max_speed)
    minute = round(((control_dist_km / max_speed) - hour) * 60)
    result = result.shift(hours=+hour, minutes=+minute)
    return result.isoformat()
    
def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if (isinstance(control_dist_km, str)):
        control_dist_km = 0.0     
    control_dist_km = abs(control_dist_km)
    result = arrow.get(brevet_start_time)
    min_speed = 0
    locations = {600:15, 1000: 11.428}
    
    if (control_dist_km > brevet_dist_km):
        speed = 0
        if (brevet_dist_km <= 600):
            speed = locations[600]
        else:
            speed = locations[1000] 
        
        hour = brevet_dist_km // speed
        minute = round(((brevet_dist_km / speed) - hour) * 60)
        if (brevet_dist_km == 200):
            result = result.shift(hours=+hour-1, minutes=+minute+10)
        else:
            result = result.shift(hours=+hour, minutes=+minute)
        return result.isoformat()

    for speed in locations:
        if control_dist_km <= speed:
            min_speed += locations[speed]
            break

    if control_dist_km <= 60:
        hour = (control_dist_km // 20)
        minute = ((control_dist_km / 20) - hour) * 60
        result = result.shift(hours=+hour, minutes=+minute)
        return result.isoformat()
    else: 
        first_sixty_hour = (60//20)
        leftover_km = (control_dist_km - 60) 
        leftover_hour = (leftover_km // min_speed)
        leftover_minute = ((leftover_km / min_speed) - leftover_hour) * 60
        total_hour = (first_sixty_hour + leftover_hour)
        result = result.shift(hours=+total_hour, minutes=+leftover_minute)
    
    if control_dist_km == 200 and brevet_dist_km == 200:
        result = result.shift(minutes=+10)
    return result.isoformat()


