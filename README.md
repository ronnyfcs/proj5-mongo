# Project 5: Brevet time calculator with Ajax and MongoDB

Simple list of controle times from project 4 stored in MongoDB database.

## ACP control times

What this software does is implement the same logic given here (https://rusa.org/octime_acp.html), but with a new technology. It'll utilize MongoDB in order to store data recevied by the user (via "Submit" button) into a data base, and displays the output on a separate page (via "Display" button) by retrieving it from the data base. This will be done through the interaction between flask and MongoDB. 


## Tasks

* Implement a "Submit" button that stores the users input to a database. 

* Implement a "Display" button that retrieves the corresponding information from the data base, and displays it on a separate webpage.

* Test the functionality of these two buttons.


## Test Cases

As mentioned in the previous project, we have designed this software to reflect the rules from (https://rusa.org/pages/acp-brevet-control-times-calculator). Also, a test file was given (with a suit of test cases) in order to test the legitimacy and functionality of the software. However, there were a few subtle corner cases that needed to be addressed and guarded against. Those cases are listed below followed by the corresponding correction implemented.

* Invalid input given in miles/km field | Correction: This will display the standard start & finish time for (0km start & open time) the given start time/date.

* Clicking "submit" without providing data | Correction: This will just send nothing to the data base. No error will return. 

* Clicking "display" without providing data | Correction: This will render a graceful error. Example: "Nothing was given. Please enter times". 

* Clicking "submit" & "display" with entering times at different time blocks (not consecutively) | Correction: This will still output all the valid times rather than empty cells.

* A special case in regards to the "display" button. If the "display" button is clicked for the first time, it will render a "try again" page. However, if you fill out some times, submit it to the data base, display it, and try to click "display" again (after hitting the back button) without entering new information, it will display the previously submitted data. So you will always have to enter new data or hit the submit button to start with a completely new/fresh data base. 


# Contributor
-------------

Ronny Fuentes <ronnyf@uoregon.edu>
